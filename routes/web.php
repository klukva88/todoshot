<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();



Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/', [
    	'as' => 'welcome',
    	'uses' => 'TaskController@show'
    	]);


    // маршрут к созданию заданий
	Route::get('create-tasks', [
		'as' => 'layouts.create-tasks',
		'uses' =>'TaskController@index'
	]);

	//маршрут к экшену сохраннения данных в бд
	 Route::post('/create-tasks', 'TaskController@store');
	
	// маршрут к настройкам пользователя
	/*Route::get('/employee-account', function () { 
    	return view('layouts.employee-account');
	});*/

	Route::resource('/employee-account', 'ProfileController');

	Route::get('task-view/{id}', [
			'as' => 'task-view.show',
			'uses' => 'TaskController@taskView'
	]);

	Route::put('task-view/{id}', [ 
		'as' => 'task-view.update',
		'uses' => 'TaskController@update' 
	]);

	Route::get('task-view/{id}/edit', [
		'as' => 'edit-task',
		'uses' => 'TaskController@edit'
		]);

	Route::put('task-view/{id}/edit', 'TaskController@updateTask');

	Route::delete('task-view/{id}', 'TaskController@deleteTask');

});

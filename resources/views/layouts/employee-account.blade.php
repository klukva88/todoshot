@extends('layouts.app')

@section('content')

<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form role="form" method="POST" action="{{ route('employee-account.update', $user->id) }}" >
				 {{ csrf_field() }}
				 {{ method_field('PUT') }}
				 	@if(Session::has('success'))
	    				<div class="alert alert-success">
	        				{{ Session::get('success') }}
	    				</div>
					@endif

					<div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
						<label class="control-label" for="exampleInputEmail1">Фамилия</label>
							<input class="form-control" id="exampleInputEmail1" name="surname" type="text" value="{{ $user->name }}">
						@if ($errors->has('surnaname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('surname') }}</strong>
                            </span>
                        @endif
					</div>

					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label class="control-label">Имя</label>
							<input class="form-control" type="text" name="name" value="{{ $user->name }}">
						@if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
					</div>

					<div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
						<label class="control-label">Отчество</label>
							<input class="form-control" type="text" name="middlename" value="{{ $user->middlename }}">
						@if ($errors->has('middlename'))
                            <span class="help-block">
                                <strong>{{ $errors->first('middlename') }}</strong>
                            </span>
                        @endif
					</div>

					<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
						<label class="control-label">Телефон</label>
							<input class="form-control" type="text" name="phone" value="{{ $user->phone }}">
						@if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
					</div>

					<div class="form-group">
						<label class="control-label">Эл. почта</label>
							<input class="form-control" type="text" disabled="disabled"></div>

					<div class="form-group">
						<label class="control-label">Отдел</label>
							<input class="form-control" type="text" disabled="disabled"></div>

				<button type="submit" class="btn btn-block btn-default">Сохранить</button>
				
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароли должны совпадать и содержать минимум 6 знаков!',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'На вашу почту отправлена ссылка для сброса пароля!',
    'token' => 'Неверный токен сброса пароля.',
    'user' => "Пользователь с данной почтой не найден",

];

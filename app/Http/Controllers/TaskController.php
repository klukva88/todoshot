<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;
use App\User;
use App\Images;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Image;

class TaskController extends Controller
{
    //рендерим html страницу создания поста
    public function index() { 
    	$users = User::all();
        $created_by = Auth::user()->id;

    	return view('layouts.create-tasks')
        ->with( compact(['users', 'created_by']));
	}

	//сохраняем данные в табличку tasks
	public function store(Request $request) {
        //получаем картинку из реквеста и присваеваем ей имя = имени загружаемого файла.
        $image =$request->file('image');
        $originalFileName = $image->getClientOriginalName();

        // добавляем и сохраняем задание(чтобы получить $task->id)
        $assigned_to = $request->assigned_to; // почему нужна объявлять переменную
		$task = Task::create([$request, 
			'title' => $request->title,
            'description' => $request->description,
            'additional' => $request->additional,
            'isUrgent' =>  $request->isUrgent,
            'assigned_to' => $assigned_to,
            'created_by' => $request->created_by,
            ]);
        $task->save();

        // сохраняем картинку в папку public (каталог виден всем)
        //$folder = Storage::makeDirectory()

        $image = $image->move(public_path('uploads/' . $task->id), $originalFileName);
        $path = 'uploads/' . $task->id . '/' . $originalFileName;
        $image = Image::make($path)->resize(400, 300);

        // сохраняем картинки в таблицу
		Images::create([$request,
			'image' => $path,
            'task_id' => $task->id,]);

		return redirect('/create-tasks')->with('success','Задание успешно добавлено');
	}


    public function show(){ //реализация вьюхи на главную страницу
        
        //dd(Task::where('assigned_to', Auth::user()->id)
            //->with('images')->get());
        $tasks = Task::where('assigned_to', Auth::user()->id)
            ->with('images')
            ->get();
            //            ->where('user_id', $task->id)

       
        return view(('welcome' ), compact(['tasks',]));
    }



    //контролер странцицы задания task-view/{id}
    public function taskView($id) {


        $task = Task::findOrFail($id);
        $task_performer = User::where( 'id', $task->assigned_to)->get();
        $task_creator = User::where('id', $task->created_by)->get();

        return view(('layouts.task-view'), compact(['task', 'task_performer', 'task_creator',]));

    }

	//апдейт задания
	public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $originalFileName = $image->getClientOriginalName();
        
        //dd(Task::find($id));
        $task = Task::find($id);
        $task->update([$request, 
            'comment' => $request->comment,
            'is_done' => $request->is_done,
            'is_tomorrow' => $request->is_tomorrow,
            ]);
        /*$task = Task::findOrFail($id);
        $task->comment = $request->comment;
        $task
        $task->save();*/

        $image = $image->move(public_path('uploads/' . $task->id), $originalFileName);
        $path = 'uploads/' . $task->id . '/' . $originalFileName;

        Images::create([$request,
            'image' => $path,
            'task_id' => $task->id,]);
        /*$image_id = Images::where('task_id', $task->id)->value('id');
        $image = Images::findOrFail($image_id);
        $image->image = $path;
        $image->save();*/
        
        

        return redirect('task-view/' . $id)->with('success','Задание успешно обновлено');
    }
     public function edit($id) {

        $task = Task::findOrFail($id);
        $users = User::all();
        // dd($task);
        return view(('layouts.edit-task'), compact(['task', 'users',]));

     }
     public function updateTask (Request $request, $id) {

        // dd($task);
        $image =$request->file('image');
        $originalFileName = $image->getClientOriginalName();
        $image = $image->move(public_path('uploads'), $originalFileName);
        $path = 'uploads/' .  $originalFileName;

// добавляем и сохраняем задание(чтобы получить $task->id)
        $assigned_to = $request->assigned_to;
        //dd(Task::find($id)); // почему нужна объявлять переменную
        $task = Task::find($id)->update([$request, 
            'title' => $request->title,
            'description' => $request->description,
            'additional' => $request->additional,
            'isUrgent' =>  $request->isUrgent,
            'assigned_to' => $assigned_to,
            ]);
        //$task->save();
        //dd($task);


// сохраняем картинки в таблицу
        $image_id = Images::where('task_id', $id)->value('id');
        $image = Images::findOrFail($image_id);
        $image->image = $path;
        $image->save();

        return redirect('task-view/' . $id)->with('success','Задание успешно изменено');
     }

     public function deleteTask ($id) {

        $task = Task::findOrFail($id);
        $images = Images::where('task_id', $task->id)->get();
         
        foreach ($images as $taskImage) {
            $taskImage->delete();
            File::delete($taskImage->image);
        }
        
        $task->delete();

        return redirect('/' );
     }

}



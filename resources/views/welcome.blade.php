@extends('layouts.app')

@section('content')
<div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1><a href="{{  url('create-tasks') }}" class="btn btn-lg btn-success" >Новая задача</a></h1>
            </div>
        </div>
     </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12 text-center">
                    <div class="btn-group">
                        <a href="#" class="btn btn-default">Все</a>
                        <a href="#" class="active btn btn-default">Срочно</a>
                        <a href="#" class="btn btn-default">Сегодня</a>
                        <a class="btn btn-default">Готовые</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
      	<div class="row">
        @foreach ( $tasks as $task )
            <div class="col-md-3 task-item">
                
                    	<a href="{{ url('task-view/' . $task->id ) }}">
                            @foreach ( $task->images as $image )
                                @if ($loop->first)
                                    <img src="{{ $image->image }}" class="img-responsive img-rounded">
                                @endif
                            @endforeach
                            <h3>{{ $task->title }}</h3>
                        </a>
               
			</div>
        @endforeach
		</div>
	</div>
</div>
@endsection




@extends('layouts.app')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
			{{-- <div class="col-md-3">
				<img class="img-responsive img-rounded" src="http://st.depositphotos.com/2313517/3165/i/950/depositphotos_31654589-stock-photo-dropped-ice-cream-cone.jpg"><a class="btn btn-block btn-default">Удалить</a>
			</div>
			<div class="col-md-3">
				<img src="http://st.depositphotos.com/2313517/3165/i/950/depositphotos_31654589-stock-photo-dropped-ice-cream-cone.jpg" class="img-responsive img-rounded"><a class="btn btn-block btn-default">Удалить</a>
			</div> --}}
		</div>
	</div>
</div>
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <form role="form" method="POST" action="{{ action( 'TaskController@updateTask', ['id' => $task->id] ) }}" enctype="multipart/form-data" >
                        {{ csrf_field() }}

				 		{{ method_field('PUT') }}
                            {{-- <input type="hidden" name="created_by" value={{$created_by}} --}}
                           {{-- @foreach($created_by as $initials)
                            "{{$initials->surname}} {{$initials->name}} {{$initials->middlename}}"
                           @endforeach >--}}

				 			<div class="form-group">
                            	<label class="control-label">Название:</label>
                            	<input class="form-control" name="title" type="text" value="{{$task->title}}">
                            </div>

                           <div class="form-group">
                                <label class="control-label">Фото</label>
                                {{-- <input type="file" multiple="multiple" name="image, image2, image3" accept="image" required="required" title="Необходимо выбрать минимум один файл" > --}}
                                <input name="image" type="file">
                                {{-- <input name="image2" type="file">
                                <input name="image3" type="file"> --}}

                            </div>

                            <div class="form-group">
                            	<label class="control-label">Описание:</label>
                            	<input class="form-control" name="description" type="text" value="{{$task->description}}">
                            </div>
                            
                            <div class="form-group">
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="isUrgent" id="optionsRadios1" value="1" checked="">Срочно
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="isUrgent" id="optionsRadios2" value="0" checked="">&nbsp;Сегодня
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Отдел</label>
                                <select class="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Сотрудник</label>
                                <select class="form-control" name="assigned_to">
                                

                                @foreach($users as $user)
                                    {<option value = "{{ $user->id }}" >{{ $user->name }} {{ $user->surname }}</option>
                                    {{-- <option>{{ $user->id }}</option> --}}
                                @endforeach

                                </select>
                            </div>
                            
                            
                        	<div class="form-group">
                        		<label class="control-label">Дополнительная информация</label>
                        		<textarea class="form-control" name="additional" type="text" value="{{$task->additional}}" placeholder="{{$task->additional}}"></textarea>
                        	</div>

                        	<button type="submit" class="btn btn-block btn-default btn-lg">Обновить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
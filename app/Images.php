<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Images extends Model
{
	use Notifiable;

	protected $table = "images";
    protected $fillable = [
        'image', 'task_id'
    ];


}


@extends('layouts.app')
@section('content')

        <div class="container">
            <div class="row">
                @if(Session::has('success'))
                     <div class="alert alert-success">
                         {{ Session::get('success') }}
                    </div>
                @endif
            </div>
        </div>    

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{ $task->title }} <a href="{{ url('task-view/' . $task->id . 'edit') }}" class="btn btn-primary">Изменить</a>
                        <form role="form" method="POST" action="{{ action('TaskController@deleteTask', ['id' => $task->id]) }}" enctype="multipart/form-data" style="display: inline;" >
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Удалить</button>
                        </form></h1> 
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Создал: {{ date('d-m-Y H:i', strtotime($task->created_at)) }}
                        <br>
                        <br>

                        @foreach ($task_creator as $details)
                            <b>{{$details->surname}} {{$details->name}} {{$details->middlename}}&nbsp;</b>(отдел проверки качества)
                            <br>
                            <br>
                            <a href="{{$details->phone}}">Телефон: {{$details->phone}}</a>
                            <br>
                            <br>
                            <br>
                        @endforeach
                    </p>
                </div>

                <div class="col-md-6">
                    <p>Исполнитель:&nbsp;
                        <br>
                        <br>
                        @foreach ($task_performer as $details)
                            <b>{{$details->surname}} {{$details->middlename}} {{$details->name}}</b>&nbsp;(клининговый отдел)
                            <br>
                            <br>
                            <a href="{{$details->phone}}">{{$details->phone}}</a>
                            <br>
                            <br>
                            <br>
                        @endforeach 
                    </p> 
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <blockquote>{{ $task->description }}</blockquote>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @foreach ( $task->images as $image )
                	<div class="col-md-3">
                        <img src="{{ asset($image->image) }}" class="img-responsive img-rounded">
                    </div>
                @endforeach
                
            </div>
        </div>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <form role="form" method="POST" action="{{ action('TaskController@update', ['id' => $task->id]) }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                        <div class="form-group">
                            <label class="control-label">Фото</label>
                            <input name="image" type="file">
                            <p class="help-block">Без фотографии выполнение задачи невозможно</p>
                        </div>

                        <div class="form-group">
                            <label class="control-label"></label>
                            <textarea class="form-control" name="comment" type="text" placeholder="Комментарий к заданию"></textarea>
                        </div>

                        <button type="submit" class="btn-lg btn-block btn-success" name="is_done" value="1">Сделано</button>
                        <div class="section">
                            <div class="container">
                                <div class="row">
                                    <button type="submit" class="btn btn-warning" name="is_tomorrow" value="1">Сделаю завтра</button>
                                </div>
                            </div>
                        </div>

                    </form>
       
                </div>
            </div>
        </div>
    </div>

    

    <script type="text/javascript" async="" src="main.js"></script>
    

@endsection
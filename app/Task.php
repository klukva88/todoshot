<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;


class Task extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "tasks";
    protected $fillable = [
        'title', 'description', 'additional', 'isUrgent', 'assigned_to', 'is_done', 'is_tomorrow', 'created_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function images() {

        return $this->hasMany('App\Images', 'task_id');
    }

   
}